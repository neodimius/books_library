<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%genre}}`.
 */
class m190824_083039_create_genre_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%genre}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        // Insert genres to DB
        $genres = array('Fantasy', 'Science fiction', 'Western', 'Romance', 'Thriller', 'Detective', 'Mystery');
        foreach ($genres as $genre) {
            $this->insert('{{%genre}}', [
                'title' => $genre,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%genre}}');
    }
}
