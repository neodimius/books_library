<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%authors}}`.
 */
class m190824_082921_create_authors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%authors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        // Insert Authors to DB
        $authors = array('Stiven King', 'Ayzek Azimov', 'Lev Tolstoy', 'Taras Shevchenko', 'Vasil Stus', 'Agata Kristi', 'Den Broun');
        foreach ($authors as $author) {
            $this->insert('{{%authors}}', [
                'name' => $author,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%authors}}');
    }
}
