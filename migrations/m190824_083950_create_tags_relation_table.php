<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tags_relation}}`.
 */
class m190824_083950_create_tags_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tags_relation}}', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer(),
            'book_id' => $this->integer(),
        ]);

        // Insert relations to DB (One books - one random tag)
        for ($i=0; $i < 9; $i++) {
            $this->insert('{{%tags_relation}}', [
                'tag_id' => rand(1, 6),
                'book_id' => $i,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tags_relation}}');
    }
}
