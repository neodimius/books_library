<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%authors}}`
 * - `{{%genre}}`
 */
class m190824_083944_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'genre_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'poster' => $this->string(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            '{{%idx-books-author_id}}',
            '{{%books}}',
            'author_id'
        );

        // add foreign key for table `{{%authors}}`
        $this->addForeignKey(
            '{{%fk-books-author_id}}',
            '{{%books}}',
            'author_id',
            '{{%authors}}',
            'id',
            'CASCADE'
        );

        // creates index for column `genre_id`
        $this->createIndex(
            '{{%idx-books-genre_id}}',
            '{{%books}}',
            'genre_id'
        );

        // add foreign key for table `{{%genre}}`
        $this->addForeignKey(
            '{{%fk-books-genre_id}}',
            '{{%books}}',
            'genre_id',
            '{{%genre}}',
            'id',
            'CASCADE'
        );

        // Insert books to DB
        for ($i=1; $i <= 10; $i++) {
            $this->insert('{{%books}}', [
                'author_id' => rand(1, 7),
                'genre_id' => rand(1, 7),
                'title' => 'Books #' . $i,
                'description' => 'Could you survive on your own, in the wild, with everyone out to make sure you don\'t live to see the morning? In the ruins of a place once known as North America lies the nation of Panem, a shining Capitol surrounded by twelve outlying districts. The Capitol is harsh and cruel and keeps the districts in line by forcing them all to send one boy and one girl between the ages of twelve and eighteen to participate in the annual Hunger Games, a fight to the death on live TV. Sixteen-year-old Katniss Everdeen, who lives alone with her mother and younger sister, regards it as a death sentence when she is forced to represent her district in the Games. But Katniss has been close to dead before - and survival, for her, is second nature. Without really meaning to, she becomes a contender. But if she is to win, she will have to start making choices that weigh survival against humanity and life against love.',
                'poster' => 'uploads/posters/' . $i . '.jpeg',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%authors}}`
        $this->dropForeignKey(
            '{{%fk-books-author_id}}',
            '{{%books}}'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            '{{%idx-books-author_id}}',
            '{{%books}}'
        );

        // drops foreign key for table `{{%genre}}`
        $this->dropForeignKey(
            '{{%fk-books-genre_id}}',
            '{{%books}}'
        );

        // drops index for column `genre_id`
        $this->dropIndex(
            '{{%idx-books-genre_id}}',
            '{{%books}}'
        );

        $this->dropTable('{{%books}}');
    }
}
