<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tags}}`.
 */
class m190824_083146_create_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tags}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        // Insert Tags to DB
        $tags = array('murder', 'love', 'aliens', 'space', 'war', 'future');
        foreach ($tags as $tag) {
            $this->insert('{{%tags}}', [
                'title' => $tag,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tags}}');
    }
}
