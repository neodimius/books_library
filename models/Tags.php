<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Tags
 * @package app\models
 */
class Tags extends ActiveRecord{
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id' => 'book_id'])
            ->viaTable('tags_relation', ['tag_id' => 'id']);
    }
}
