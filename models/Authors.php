<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Authors
 * @package app\models
 */
class Authors extends ActiveRecord{
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['author_id' => 'id']);
    }
}