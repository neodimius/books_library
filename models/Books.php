<?php

namespace app\models;

use phpDocumentor\Reflection\DocBlock\Tags\Author;
use yii\db\ActiveRecord;

/**
 * Class Books
 * @package app\models
 */
class Books extends ActiveRecord
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['id' => 'tag_id'])
            ->viaTable('tags_relation', ['book_id' => 'id']);
    }
}
