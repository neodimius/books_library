<?php
namespace app\models;

use Yii;
use yii\base\Model;

class AddBookForm extends Model
{
    public $author_id;
    public $genre_id;
    public $title;
    public $description;
    public $poster;
    public $tags;

    public function rules() {

        return [
            [['author_id', 'genre_id', 'title', 'description'], 'required'],
            [['author_id', 'genre_id'], 'number'],
            [['poster'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = Yii::getAlias('uploads/posters/') . $this->title . '_' . date('dmy') . $this->poster->extension; // . $this->file->baseName . '.' . $this->file->extension;
            $this->poster->saveAs($path);
            return $path;
        } else {
            return false;
        }
    }
}