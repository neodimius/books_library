<?php
/* @var $this yii\web\View */

$this->title = $book['title'];
?>
<div class="row">
    <div class="col-md-9">
        <div class="jumbotron mb-3">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="<?= $book['poster'] ?>" class="card-img" alt="<?= $book['title'] ?>">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= $book['title'] ?></h5>
                        <p class="card-text"><?= $book['description'] ?></p>
                        <a class="btn btn-info" href="<?= Yii::$app->homeUrl . '?author=' . $author['id'] ?>"><?= $author['name'] ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="alert alert-danger">Genre:
            <a class="btn btn-warning" href="<?= Yii::$app->homeUrl . '?genre=' . $genre['id'] ?>"><?= $genre['title'] ?></a>
        </div>
        <!--Tags:        -->
        <div class="alert alert-success">Tags:</div>
        <div>
            <?php foreach ($tags as $tag):?>
                <a class="btn btn-success" href="<?= Yii::$app->homeUrl . '?tag=' . $tag['id'] ?>"><?= $tag['title'] ?></a>
            <?php endforeach;?>
        </div>
    </div>
</div>