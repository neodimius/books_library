<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Add new book';
?>

<h1><?= $this->title ?></h1>

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'options' => ['enctype' => 'multipart/form-data'],
    'action' => Url::to(['site/add-book']),
]) ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'description') ?>
<?= $form->field($model, 'genre_id')->dropdownList(
    $genres, ['prompt'=>'Select Genre']);
?>
<?= $form->field($model, 'author_id')->dropdownList(
    $authors, ['prompt'=>'Select Author']);
?>
<?= $form->field($model, 'tags[]')->checkboxList($tags)->label('Tags:') ?>
<?= $form->field($model, 'poster')->fileInput() ?>
<?= Html::submitButton('Add book', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>

