<?php
/* @var $this yii\web\View */

$this->title = 'Books library';
?>
    <h1>Books Library</h1>
<div class="row">
    <div class="col-md-9">
        <!--Genres:        -->
        <div class="d-flex justify-content-between">
            <div class="alert alert-danger">Genres:
                <?php foreach ($genres as $genre):?>
                    <a class="btn btn-warning" href="<?= Yii::$app->homeUrl . '?genre=' . $genre['id'] ?>"><?= $genre['title'] ?></a>
                <?php endforeach;?>
            </div>
        </div>

        <!--Books:-->
        <!--Order by        -->
        <div class="">
            <span class="bg-success">Order by:</span>
            <a class="badge badge-dark" href="<?= Yii::$app->homeUrl . '?order=title' ?>">title</a>
            <a class="badge badge-dark" href="<?= Yii::$app->homeUrl . '?order=id' ?>">id</a>
        </div>
            <div class="card-deck">
            <?php foreach ($books as $book):?>
                <a href="<?= \yii\helpers\Url::to(['site/book', 'id' => $book['id']]); ?>">
                    <div class="card">
                        <img src="<?= $book['poster'] ?>" class="card-img-top" alt="<?= $book['title'] ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $book['title'] ?></h5>
                            <p class="card-text"><?= substr($book['description'], 0, 100) . '...' ?></p>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>
            </div>
            <?php echo \yii\bootstrap4\LinkPager::widget([
                'pagination' => $pagination,
            ]);?>
    </div>
    <div class="col-md-3">
        <a class="btn btn-danger" href="<?= \yii\helpers\Url::to(['site/add-book']); ?>">Add new book</a>
        <!--Tags:        -->
        <div class="alert alert-success">Tags:</div>
        <div>
            <?php foreach ($tags as $tag):?>
                <a class="btn btn-success" href="<?= Yii::$app->homeUrl . '?tag=' . $tag['id'] ?>"><?= $tag['title'] ?></a>
            <?php endforeach;?>
        </div>

        <!--Authors:        -->
        <div class="alert alert-info">Authors:</div>
        <div>
            <?php foreach ($authors as $author):?>
                <a class="btn btn-primary" href="<?= Yii::$app->homeUrl . '?author=' . $author['id'] ?>"><?= $author['name'] ?></a>
            <?php endforeach;?>
        </div>
    </div>
</div>
