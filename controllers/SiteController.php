<?php

namespace app\controllers;

use app\models\AddBookForm;
use app\models\Authors;
use app\models\Books;
use app\models\Genre;
use app\models\Tags;
use app\models\Tags_relation;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $request = Yii::$app->request;

        //Get books:
        $query = $request->get('genre') ?   : Books::find();
        if ($request->get('genre')) {
            $query = Books::find()->where(['genre_id' => $request->get('genre')]);
        }elseif ($request->get('tag')) {
            $query = Tags::findOne(['id' => $request->get('tag')])->getBooks();
        }elseif ($request->get('author')) {
            $query = Authors::findOne(['id' => $request->get('author')])->getBooks();
        } else {
            $query = Books::find();
        }
        $count = is_array($query) ? count($query) : $query->count();
        $pagination = new Pagination ([
            'defaultPageSize' => 5,
            'totalCount' => $count,
        ]);

        $order = $request->get('order') ?: 'id';
        $books = $query -> orderBy($order)
            -> offset($pagination->offset)    //Query of teams table
            -> limit ($pagination->limit)
            -> all();

        //Get Genres
        $genres = Genre::find()->orderBy('id')->all();

        //Get Tags
        $tags = Tags::find()->orderBy('id')->all();

        //Get Authors
        $authors = Authors::find()->orderBy('name')->all();

        return $this -> render('index', [
            'books' => $books,
            'pagination' => $pagination,
            'genres' => $genres,
            'tags' => $tags,
            'authors' => $authors,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionBook()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $request = Yii::$app->request;
        $book = Books::findOne($request->get('id'));
        $author = Authors::findOne($book['author_id']);
        $genre = Genre::findOne($book['genre_id']);
        $tags = $book->getTags()->all();

        return $this->render('book', [
            'book' => $book,
            'author' => $author,
            'genre' => $genre,
            'tags' => $tags,
        ]) ;
    }

    public function actionAddBook()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $form = new AddBookForm();

        if (Yii::$app->request->isPost) {
            if ($form->load(Yii::$app->request->post())) {
                $form->poster = UploadedFile::getInstance($form, 'poster');
                $poster = $form->upload();

                // Write book to DB
                $book = new Books();
                $book->title = $form->title;
                $book->description = $form->description;
                $book->author_id = $form->author_id;
                $book->genre_id = $form->genre_id;
                $book->poster = $poster;
                $book->save();

                // Record Tags
                $tags = Yii::$app->request->post('AddBookForm')['tags'];
                foreach ($tags as $tag) {
                    $record = new Tags_relation();
                    $record->tag_id = $tag;
                    $record->book_id = $book->id;
                    $record->save();
                }

                return $this->redirect(['site/index']);
            }
        }

        $authors = Authors::find()->select(['name', 'id'])->indexBy('id')->column();
        $genres = Genre::find()->select(['title', 'id'])->indexBy('id')->column();
        $tags = Tags::find()->select(['title', 'id'])->indexBy('id')->column();
        return $this->render('addBook', [
            'model' => $form,
            'authors' => $authors,
            'genres' => $genres,
            'tags' => $tags,
        ]);
    }
}
